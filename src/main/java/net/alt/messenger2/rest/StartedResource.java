package net.alt.messenger2.rest;

import net.alt.messenger2.service.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/hi")
public class StartedResource {

    @Autowired
    private Sender sender;

    @GetMapping("/")
    public String getAllCodes() {
        sender.send("selector.q","hi");
        return "ok";
    }
}


